const express = require('express');
const cors = require('cors');
const app = express();

const auth = require('./routes/auth');
const users = require('./routes/users');
const orgs = require('./routes/orgs');
const notifications = require('./routes/notifications');
const devices = require('./routes/devices');

const handleError = require('./middleware/error');
const { validateAuthToken } = require('./middleware/validate-auth-token');

require('./errors/handle-process-errors')();

app.use(
  cors({
    origin: true,
  }),
);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.get('/', (req, res) => res.status(200).send('Welcome to the Notifyd Api'));
app.use('/api/auth', auth);
app.use('/api/devices', devices);
app.use('/api/users', [ validateAuthToken ], users);
app.use('/api/organizations', [ validateAuthToken ], orgs);
app.use('/api/notifications', [ validateAuthToken ], notifications);

app.use(handleError);

const port = process.env.PORT || 8000;

app.listen(port, () => console.debug(`Now listening on ${port}`));
