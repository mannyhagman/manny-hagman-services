const { usersCollection, orgsCollection } = require('../../../config/constants');
const { db } = require('../../../config/firestore');

async function sendNotification(req, res) {
  const { organization, responseURL } = req.body;
  const { users, actions, title, message } = req.body.notification;
  const orgDocRef = orgsCollection.doc(res.locals.organization).collection('notifications').doc();
  const { id } = orgDocRef;
  const batch = db.batch();

  try {
    users.forEach((user) => {
      batch.set(orgDocRef, {
        id,
        data: {
          title,
          message,
          id,
        },
        actions: actions || [],
        responseURL,
        organization,
        timestamp: new Date(),
        adminId: req.body.adminId,
      });
      const usersColNotifDocRef = usersCollection.doc(user).collection('notifications').doc(id);
      const usersColNotif = orgsCollection
        .doc(organization)
        .collection('users')
        .doc(user)
        .collection('notifications')
        .doc(id);
      batch.set(usersColNotifDocRef, {
        id,
        data: {
          title,
          message,
          id,
        },
        actions: actions || [],
        responseURL,
        organization,
        adminId: req.body.adminId,
        timestamp: new Date(),
      });
      batch.set(usersColNotif, {
        id,
        data: {
          title,
          message,
          id,
        },
        actions: actions || [],
        responseURL,
        organization,
        adminId: req.body.adminId,
        timestamp: new Date(),
      });
    });
    const notificationRootColDoc = db.collection('notifications').doc(id);

    batch.set(notificationRootColDoc, {
      id,
      data: {
        title,
        message,
        id,
      },
      adminId: req.body.adminId,
      actions: actions || [],
      responseURL,
      organization,
      timestamp: new Date(),
    });

    return batch.commit().then((result) => {
      res.status(200).send({
        message: 'success',
        responseURL,
        notification: {
          recipients: users.length,
          id,
        },
        errors: [],
      });
    });
  } catch (error) {
    res.status(500).send(error.message);
  }
}

exports.sendNotification = sendNotification;
