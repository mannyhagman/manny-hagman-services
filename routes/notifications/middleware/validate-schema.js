const Joi = require('joi');

const notificationSchema = {
  notification: Joi.object({
    title: Joi.string(),
    message: Joi.string(),
    actions: Joi.array(),
    users: Joi.array().required(),
  }),
  organization: Joi.string().required(),
  responseURL: Joi.string().required(),
};

function validateSchema(req, res, next) {
  const { notification, organization, responseURL } = req.body;
  const { error } = Joi.validate(
    {
      notification,
      organization,
      responseURL,
    },
    notificationSchema,
  );
  if (!error) {
    res.locals = req.body;
    next();
  } else {
    console.error(error);
    res.status(400).send(error.details[0].message);
  }
}

exports.validateSchema = validateSchema;
