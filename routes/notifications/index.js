const express = require('express');
const router = express.Router();
const { verifyOrganization } = require('../../middleware/verify-organization');
const { sendNotification } = require('../notifications/middleware/send-notification');
const { validateSchema } = require('./middleware/validate-schema');

router.get('/', (req, res) => {
  res.send('notifications resource');
});

router.post('/', [ validateSchema, verifyOrganization, sendNotification ]);
router.get('/', [ validateSchema, verifyOrganization, sendNotification ]);

module.exports = router;
