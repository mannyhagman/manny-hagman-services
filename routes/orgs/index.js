const express = require('express');
const router = express.Router();

const { validateSchema } = require('./middleware/validate-schema');
const { verifyOrganization } = require('../../middleware/verify-organization');
const { createOrganization } = require('./middleware/create-organization');
const { getAuthUsers } = require('./middleware/get-auth-users');
const { getUsers } = require('./middleware/get-users');

router.get('/', (req, res) => {
  res.send('orgs resource');
});

router.get('/:organization/users/authorized', [ verifyOrganization, getAuthUsers ]);
router.get('/:organization/users/', [ verifyOrganization, getUsers ]);

router.post('/', [ validateSchema, createOrganization ]);

module.exports = router;
