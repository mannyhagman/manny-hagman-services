const { orgsCollection, usersCollection, orgs, users } = require('../../../config/constants');

async function moveUsers(req, res, next) {
  try {
    const query = await usersCollection.where('organization', '==', '2sBmbiveJbOo6UNZgwjQ').get();
    const queryResult = await query.docs.map((user) =>
      orgsCollection.doc('2sBmbiveJbOo6UNZgwjQ').collection(users).doc(user.id).set(user.data()),
    );
    console.debug(queryResult);
  } catch (error) {
    next(error);
  }
}

exports.moveUsers = moveUsers;
