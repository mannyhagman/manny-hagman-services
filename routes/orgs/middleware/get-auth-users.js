const { orgsCollection, authorized, tempToken, users } = require('../../../config/constants');

async function getAuthUsers(req, res, next) {
  try {
    const orgDocs = await orgsCollection.doc(res.locals.organization).collection(users).get();
    const orgs = [];
    orgDocs.forEach((item) => orgs.push(item.data()));
    res.send({
      code: 200,
      message: 'Successfully retrieved users authorized for messages',
      users: orgs.filter((item) => item.expoDeviceToken).map((item) =>
        Object.assign({
          uid: item.uid,
          email: item.email,
          deviceToken: item.expoDeviceToken,
          organization: item.organization,
          authorized: item.authorized,
        }),
      ),
    });
  } catch (error) {
    console.error(error);
    next(error);
  }
}

exports.getAuthUsers = getAuthUsers;
