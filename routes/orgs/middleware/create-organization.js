const { orgsCollection, adminsCollection, orgs } = require('../../../config/constants');
const { db } = require('../../../config/firestore');

async function createOrganization(req, res, next) {
  try {
    const { adminId, name, email } = res.locals;
    const batch = db.batch();
    const orgDocRef = orgsCollection.doc();

    batch.set(orgDocRef, {
      org_id: orgDocRef.id,
      admin: adminId,
      name,
      contact: email,
      enabled: true,
      authorized: true,
      timestamp: new Date(),
    });

    const adminOrgDocRef = adminsCollection.doc(adminId).collection(orgs).doc(orgDocRef.id);

    batch.set(adminOrgDocRef, {
      org_id: orgDocRef.id,
      admin: adminId,
      name,
      contact: email,
      enabled: true,
      authorized: true,
      timestamp: new Date(),
    });

    batch.commit().then(() => {
      res.status(200).send({
        message: `success, ${name} created`,
        organization: `${orgDocRef.id}`,
        timestamp: new Date().toUTCString(),
      });
      return null;
    });
  } catch (error) {
    res.send(error.message);
  }
}

exports.createOrganization = createOrganization;
