const { orgsCollection, users } = require('../../../config/constants');

async function getUsers(req, res, next) {
  try {
    console.debug(`Getting list of users for ${req.params.organization}`);

    const orgUsersSubCollection = await orgsCollection.doc(req.params.organization).collection(users).get();
    const usersList = orgUsersSubCollection.docs.map((item) => item.data());

    res.status(200).send({
      code: 200,
      message: `Successfully all users for organization: ${req.params.organization}`,
      usersList,
      numUsers: usersList.length,
    });
  } catch (error) {
    next(error);
  }
}

exports.getUsers = getUsers;
