const Joi = require('joi');

const organizationSchema = {
  email: Joi.string().email().required(),
  name: Joi.string().required(),
};

function validateSchema(req, res, next) {
  const { name, email } = req.body;
  const { error } = Joi.validate(
    {
      email,
      name,
    },
    organizationSchema,
  );
  if (!error) {
    res.locals.name = name;
    res.locals.email = email;
    next();
  } else {
    res.send(error.details[0].message);
  }
}

exports.validateSchema = validateSchema;
