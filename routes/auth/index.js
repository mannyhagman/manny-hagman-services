const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router();
const { SECRET, TOKENTIME, adminsCollection, orgs } = require('../../config/constants');
const firebase = require('../../config/firebase');

router.post('/', async (req, res) => {
  try {
    const { email, password, permanent } = req.body;
    const { user: { uid } } = await firebase.auth().signInWithEmailAndPassword(email, password);
    res.locals.uid = uid;
    const adminDocRef = adminsCollection.doc(uid);
    const adminDoc = await adminDocRef.get();

    if (!adminDoc.exists) {
      console.error('User is not a Notifyd Admin. Access Denied');
      res.status(400).send(`User: ${uid} is not a Notifyd Admin. Access Denied`);
      return;
    } else {
      const accessToken = jwt.sign({ id: uid }, SECRET, { expiresIn: TOKENTIME });
      if (!permanent) {
        console.debug(`POST /auth','req.body: ${req.body}`);
        refreshToken = null;
      } else {
        refreshToken = `${uid}.${accessToken}`;
        adminDocRef.update({
          refreshToken,
        });
      }

      const adminOrgsRef = await adminsCollection.doc(uid).collection(orgs).get();
      const organizations = adminOrgsRef.docs.map((item) => item.id);

      console.debug(`Access Granted. API credentials generated for admin email:${email} adminId:${uid}`);
      res.status(200).send({
        message: `Access Granted. API credentials generated for admin email:${email} adminId:${uid}`,
        accessToken,
        refreshToken,
        organizations,
      });
    }
  } catch ({ name, message }) {
    console.error(`${name} ${message}`);
    res.status(500).send(`${name} ${message}`);
  }
});

module.exports = router;
