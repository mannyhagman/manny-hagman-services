const express = require('express');
const router = express.Router();

const { saveExpoDeviceToken } = require('./middleware/save-device-token');

router.get('/', (req, res, next) => {
  res.send('devices service');
});

router.post('/', [ saveExpoDeviceToken ]);

module.exports = router;
