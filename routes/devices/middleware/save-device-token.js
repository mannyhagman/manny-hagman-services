const { usersCollection, orgsCollection, users } = require('../../../config/constants');
const { db } = require('../../../config/firestore');

const saveExpoDeviceToken = async (req, res, next) => {
  const { expoDeviceToken, uid } = req.body;
  try {
    const batch = db.batch();
    const userDocRef = usersCollection.doc(uid);
    const userDoc = await userDocRef.get();
    const orgId = userDoc.data().organization;
    const orgUserDocRef = orgsCollection.doc(orgId).collection(users).doc(uid);
    batch.update(userDocRef, {
      expoDeviceToken,
    });
    batch.update(orgUserDocRef, {
      expoDeviceToken,
    });
    const batchResult = await batch.commit();
    if (batchResult.length !== 2) {
      throw new Error('Batch not successful');
    }
    return res.send({
      code: 201,
      message: 'Successfuly updated user device token ',
    });
  } catch ({ name, message }) {
    res.send(`${name}: ${message}`);
  }
};

exports.saveExpoDeviceToken = saveExpoDeviceToken;
