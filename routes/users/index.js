const express = require('express');
const { validateSchema } = require('./middleware/validate-schema');
const { sendConfirmation } = require('./middleware/send-confirmation');
const { addUser } = require('./middleware/add-user');
const { createUser } = require('./middleware/create-user');
const { updateUser } = require('./middleware/update-user');
const { updatePassword } = require('./middleware/update-password');

const { verifyUser } = require('./middleware/verify-user');
const { deleteUserAuth } = require('./middleware/delete-user-auth');
const { deleteUserDoc } = require('./middleware/delete-user-doc');
const { getUsers } = require('../orgs/middleware/get-users');
const { verifyOrganization } = require('../../middleware/verify-organization');
const { resendUserEmail } = require('../users/middleware/resend-user-email');
const router = express.Router();

router.put('/:id', updateUser);
router.post('/random', updatePassword);
router.post('/', [ validateSchema, createUser, addUser, sendConfirmation ]);

router.post('/email', resendUserEmail);
router.delete('/:organization/:id', [ verifyUser, deleteUserAuth, deleteUserDoc ]);
router.get('/:organization', [ verifyOrganization, getUsers ]);

module.exports = router;
