const nodemailer = require('nodemailer');
const { auth } = require('../../../config/firestore');
const { emailSubject, emailSender, infoEmail, infoEmailPassword } = require('../../../config/constants');

async function resendUserEmail(req, res, next) {
  try {
    const { email } = await auth.getUser(req.body.uid);
    const passwordResetLink = await auth.generatePasswordResetLink(email);
    const resendUserEmailTemplate = `<p>Hello,</p>
<p>Follow this link to verify your email address.</p>
<p><a href='${passwordResetLink}'>Reset Password</a></p>
<p>If you didn’t ask to verify this address, you can ignore this email.</p>
<p>Thanks,</p>
<p>Your Notifyd team</p>`;
    const mailTransport = nodemailer.createTransport(`smtps://${infoEmail}:${infoEmailPassword}@smtp.gmail.com`);
    const options = { from: emailSender, to: email, subject: emailSubject, html: resendUserEmailTemplate };
    const result = await mailTransport.sendMail(options);
    console.debug(result);
    res.status(200).send({
      code: 200,
      message: 'Successfully sent reset password email',
    });
  } catch (error) {
    next(error);
  }
}

exports.resendUserEmail = resendUserEmail;
