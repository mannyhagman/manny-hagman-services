const Joi = require('joi');

const userSchema = {
  email: Joi.string().email().required(),
  organization: Joi.string().required(),
  confirmation: Joi.boolean(),
  displayName: Joi.string(),
  adminId: Joi.string().allow(),
};

function validateSchema(req, res, next) {
  const { email, organization, confirmation, displayName } = req.body;
  const { error } = Joi.validate(
    {
      email,
      organization,
      confirmation,
      displayName,
      adminId: req.body.adminId,
    },
    userSchema,
  );
  if (!error) {
    next();
  } else {
    res.send(error);
  }
}

exports.validateSchema = validateSchema;
