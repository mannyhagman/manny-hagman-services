const { db } = require('../../../config/firestore');
const { usersCollection, orgsCollection } = require('../../../config/constants');

async function deleteUserDoc(req, res, next) {
  try {
    const { uid } = res.locals;
    console.debug('delete userDoc function started');
    const userOrgDoc = await usersCollection.doc(uid).get();
    const userOrg = userOrgDoc.data().organization;

    console.debug(`User with uid:${uid} is part of org:${userOrg}`);
    const batch = db.batch();

    batch.delete(usersCollection.doc(req.params.id));
    batch.delete(orgsCollection.doc(userOrg).collection('users').doc(req.params.id));

    const result = await batch.commit();

    if (result.length === 2) {
      res.status(201).send({
        code: 201,
        message: `Successfully deleted user from AuthDB, Users Collection and ${userOrg}: users collection`,
      });
    }
  } catch (error) {
    next(error);
  }
}

exports.deleteUserDoc = deleteUserDoc;
