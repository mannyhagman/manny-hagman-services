const { auth } = require('../../../config/firestore');
const { orgsCollection, usersCollection } = require('../../../config/constants');

async function deleteUserAuth(req, res, next) {
  try {
    const { id } = req.params;
    console.debug('starting delete user auth function');
    auth.deleteUser(id).then(async () => {
      const userDoc = await usersCollection.doc(id).get();
      console.log(userDoc.exists);
      if (userDoc.exists) {
        console.debug('passing control to deleteUserDoc');
        next();
      } else {
        res.send({
          code: 201,
          message: `User successfully deleted from authDB`,
        });
      }
    });
  } catch (error) {
    next(error);
  }
}

exports.deleteUserAuth = deleteUserAuth;
