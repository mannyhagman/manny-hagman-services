const crypto = require('crypto');
const { auth } = require('../../../config/firestore');

async function createUser(req, res, next) {
  console.log(req.body, 'at createUser');
  const { email, displayName } = res.locals;
  const password = crypto.randomBytes(8).toString('hex');
  try {
    const { uid, disabled } = await auth.createUser({
      email,
      displayName,
      password,
      displayName,
    });
    res.locals.uid = uid;
    res.locals.displayName = displayName;
    res.locals.disabled = disabled;
    res.locals.password = password;
    next();
  } catch (error) {
    console.error(error, error.message);
    res.send(error);
  }
}

exports.createUser = createUser;
