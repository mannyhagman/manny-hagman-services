const { auth } = require('../../../config/firestore');

async function getUserById(req, res, next) {
  try {
    const user = await auth.getUser(req.params.id);
    console.debug(user.email);
  } catch (error) {
    next(error);
  }
}

exports.getUserById = getUserById;
