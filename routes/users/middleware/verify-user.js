const { auth } = require('../../../config/firestore');
const { usersCollection } = require('../../../config/constants');

async function verifyUser(req, res, next) {
  try {
    console.debug('verifying user exists in firebase auth database');
    const { id } = req.params;
    console.debug(`Request params for verifyUser function are:`, req.params);
    console.debug(`uid being checked in verifyUser is: ${id}`);
    const { uid } = await auth.getUser(id);
    res.locals.uid = uid;
    const userDoc = await usersCollection.doc(id).get();
    console.debug(`Does user with ${uid} have a Users collection document:? ${userDoc.exists ? 'Yes' : 'No'}`);
    console.debug(`User with ${uid} found, passing control deleteUserAuth`);
    next();
  } catch (error) {
    next(error);
  }
}

exports.verifyUser = verifyUser;
