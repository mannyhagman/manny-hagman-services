const { auth } = require('../../../config/firestore');
const { usersCollection, orgsCollection, users } = require('../../../config/constants');
const { db } = require('../../../config/firestore');

const updateUser = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { email } = req.body;

    if (!id) {
      res.status(400).send({
        code: 400,
        error: 'No uid provided, Bad request',
      });
      return;
    }

    if (!email) {
      res.status(400).send({
        code: 400,
        error: 'No email provided, Bad request',
      });
      return;
    }

    const result = await auth.updateUser(id, {
      email,
    });

    if (result.email !== email) {
      res.send('Unable to update email');
      return;
    }

    const batch = db.batch();

    batch.update(usersCollection.doc(id), {
      email,
    });

    const userDocRef = await usersCollection.doc(id).get();
    const userOrg = userDocRef.data().organization;

    batch.update(orgsCollection.doc(userOrg).collection(users).doc(id), {
      email,
    });
    const batchResult = await batch.commit();

    res.send({
      code: 200,
      message: `User email has been successfully updated to ${email}`,
    });
    return batchResult;
  } catch (error) {
    next(error);
  }
};

exports.updateUser = updateUser;
