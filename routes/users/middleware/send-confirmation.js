const nodemailer = require('nodemailer');

const { emailSubject, emailSender, infoEmail, infoEmailPassword } = require('../../../config/constants');

async function sendConfirmation(req, res, next) {
  try {
    const mailTransport = nodemailer.createTransport(`smtps://${infoEmail}:${infoEmailPassword}@smtp.gmail.com`);
    const { email, password, displayName, organization, uid } = res.locals;
    const options = {
      from: emailSender,
      to: email,
    };
    options.subject = emailSubject;
    options.html = `<h2 style="color:#00131E">Hello!</h2>
	<p> Your organization has invited you to use the Notifyd Device App so that you can keep track of shift requests</p>
	<p> All you need to do is download this free mobile app and log in. Simple, right? Get started today!</p>
  <p> Step 1: Download the free app here: <b><a style="color:#00131E" href='https://itunes.apple.com/us/app/notifyd-device/id1281731159?mt=8'> iPhone </a> </b>or
  <b><a style="color:#00131E" href='https://play.google.com/store/apps/details?id=com.notifyd.device'>Android</a> </b>
  </p>
	<p> Step 2: Open the App </p>
  <p>Step 3: On first open of the app, click Need to Signup Instead?. </p>
  <p>Step 4: Enter your email and temporary password.</p>
	<ul>
		<li>email: ${email}</li>
		<li>password: ${password}</li>
	</ul>
  <p>Step 5: Complete the sign up process and log in! </p>
	<p>Don’t miss out! Make sure to allow push notifications when prompted. No push notifications, no worries! All notifications from your organization will still be located in the Notifyd app for your review and response options.</p>
	<p>Sit back, relax, and let the opportunities come to you!</p>
  <p>Need help? Reach out to us anytime at support@notifyd.com</p>`;
    const result = await mailTransport.sendMail(options);
    if (result.accepted.length > 0) {
      res.status(200).send({
        message: 'successfully authenticated user',
        displayName,
        organization,
        uid,
        authorized: true,
      });
      return;
    } else {
      res.status(200).send({
        message: 'successfully authenticated user',
        displayName,
        organization,
        uid,
        authorized: true,
      });
      return;
    }
  } catch (error) {
    res.send(error.message);
  }
}

exports.sendConfirmation = sendConfirmation;
