const { db } = require('../../../config/firestore');
const { usersCollection, orgsCollection, users } = require('../../../config/constants');

async function addUser(req, res, next) {
  const { uid, email, disabled, displayName, adminId, organization } = res.locals;

  try {
    const batch = db.batch();
    const orgDocRef = orgsCollection.doc(organization).collection(users).doc(uid);
    batch.set(orgDocRef, {
      authorized: true,
      uid,
      email,
      disabled,
      displayName,
      adminId,
      organization,
      timestamp: new Date(),
    });
    const userDocRef = usersCollection.doc(uid);
    batch.set(userDocRef, {
      authorized: true,
      uid,
      email,
      disabled,
      displayName,
      adminId,
      organization,
      timestamp: new Date(),
    });
    batch.commit().then(() => {
      next();
      return null;
    });
  } catch (error) {
    res.send(error.stack);
  }
}

exports.addUser = addUser;
