const { auth } = require('../../../config/firestore');

const updatePassword = async (req, res, next) => {
  const { password, uid } = req.body;
  if (!password) {
    res.status(500).send({
      code: 500,
      message: 'Bad request, no password provided',
    });
  }
  if (!uid) {
    res.status(500).send({ code: 500, message: 'Bad request, no uid provided' });
  }
  try {
    const result = await auth.updateUser(uid, {
      password,
    });
    res.status(200).send({
      code: 200,
      message: 'Password update successful',
    });
  } catch (error) {
    next(error);
  }
};

exports.updatePassword = updatePassword;
