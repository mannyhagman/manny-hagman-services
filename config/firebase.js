const firebase = require('firebase');
const { firebaseConfigProd } = require('./constants');

module.exports = firebase.initializeApp(firebaseConfigProd);
