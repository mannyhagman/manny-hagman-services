const admin = require('firebase-admin');
exports.FieldValue = admin.firestore.FieldValue;

admin.initializeApp({
  credential: admin.credential.cert(require('../service-account.json')),
  databaseURL: 'https://notifydapp-6b6a8.firebaseio.com',
});

admin.firestore().settings({
  timestampsInSnapshots: true,
});

const db = admin.firestore();
const auth = admin.auth();

module.exports = {
  db,
  auth,
};
