const functions = require('firebase-functions');
const app = require('./app');
const { handleCreateNotification, handleCreateMessage } = require('./app/listeners/listeners-notifications');
exports.api = functions.https.onRequest(app);
exports.handleCreateNotification = handleCreateNotification;
exports.handleCreateMessage = handleCreateMessage;
