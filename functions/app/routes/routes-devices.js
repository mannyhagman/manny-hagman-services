const express = require('express');
const db = require('../db');
const router = express.Router();

const { saveExpoDeviceToken } = require('../middlewares/middlewares-devices')(db);

router.get('/', (req, res, next) => {
	res.send('devices service');
});

router.post('/', [ saveExpoDeviceToken ]);

module.exports = router;
