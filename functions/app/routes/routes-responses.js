const express = require('express');

const router = express.Router();
const db = require('../db');

const {
	getAdminId,
	createHeader,
	createResponseObj,
	createAgent,
	updateResponse,
	createResponsesCollection,
	postResponse,
} = require('../middlewares/middlewares-responses')(db);

router.get('/', (req, res, next) => {
	res.send('responses resource');
});

router.post('/', [
	getAdminId,
	createHeader,
	createResponseObj,
	createAgent,
	createResponsesCollection,
	updateResponse,
	postResponse,
]);

module.exports = router;
