const https = require('https');
const axios = require('axios');
const { orgs, admins, notifications, FieldValue } = require('../constants');

module.exports = function(db) {
  const getAdminId = async (req, res, next) => {
    res.locals.organization = req.body.orgId;
    try {
      const orgDocRef = db.collection(orgs).doc(req.body.orgId);
      const result = await orgDocRef.get();
      res.locals.adminId = result.data().admin;
      next();
    } catch (error) {
      res.send({
        error: error.message,
      });
    }
  };

  const createHeader = async (req, res, next) => {
    try {
      const adminDocRef = db.collection(admins).doc(res.locals.adminId);
      const results = await adminDocRef.get();

      res.locals.headers = { 'Content-Type': 'application/json', ...results.data().responseAuth };
      next();
    } catch (error) {
      res.send({
        error: error.message,
      });
    }
  };

  const createResponseObj = async (req, res, next) => {
    try {
      res.locals.notificationData = req.body;
      next();
    } catch (error) {
      res.send({
        error: error.message,
      });
    }
  };

  const createAgent = async (req, res, next) => {
    try {
      res.locals.httpsAgentt = new https.Agent({
        rejectUnauthorized: false,
        keepAlive: true,
      });
      next();
    } catch (error) {
      res.send({
        error: error.message,
      });
    }
  };
  const updateResponse = async (req, res, next) => {
    try {
      const { notificationID, response, uid } = res.locals.notificationData;
      const notifRef = db.collection(orgs).doc(res.locals.organization).collection(notifications).doc(notificationID);
      notifRef
        .update({
          responses: FieldValue.arrayUnion(Object.assign({ uid, response })),
        })
        .then((result) => console.debug(result));
      next();
    } catch (error) {
      res.send(error.message);
    }
  };

  const createResponsesCollection = (req, res, next) => {
    try {
      const { notificationID, response, uid } = res.locals.notificationData;
      db.collection(uid + '_Responses').doc(`${notificationID}_${response}`);
      next();
    } catch ({ name, message }) {
      res.send(`Notifyd Error ${name}: ${message}`);
    }
  };

  const postResponse = async (req, res, next) => {
    try {
      const { responseURL } = req.body;
      const { notificationID, response, uid } = res.locals.notificationData;
      const { headers } = res.locals;
      const { httpsAgent } = res.locals;

      const result = await axios.post(
        responseURL,
        {
          notificationID,
          uid,
          response,
          organization: res.locals.organization,
        },
        {
          headers,
          httpsAgent,
        },
      );
      res.send(result.data);
    } catch (error) {
      res.send({
        error: error.message,
      });
    }
  };

  return {
    getAdminId,
    createHeader,
    createResponseObj,
    createAgent,
    createResponsesCollection,
    updateResponse,
    postResponse,
  };
};
