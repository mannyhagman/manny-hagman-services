const https = require('https');
const { users } = require('../constants');

module.exports = function(db) {
	const saveExpoDeviceToken = async (req, res, next) => {
		const { expoDeviceToken, uid } = req.body;
		try {
			db
				.collection(users)
				.doc(uid)
				.update({
					expoDeviceToken: expoDeviceToken
				})
				.then((result) => res.status(201).send(result))
				.catch((error) => console.error(error));
		} catch ({ name, message }) {
			res.send(`${name}: ${message}`);
		}
	};
	return {
		saveExpoDeviceToken
	};
};
