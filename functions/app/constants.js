const db = require('./db');

const admins = 'admins';
const orgs = 'organizations';
const users = 'users';
const notifications = 'notifications';

const usersCollection = db.collection(users);
const adminsCollection = db.collection(admins);
const notifsCollection = db.collection(notifications);
const orgsCollection = db.collection(orgs);

const userNotifsDocPath = '/organizations/{orgId}/users/{uid}/notifications/{notificationId}';
const expoNotificationServiceUrl = 'https://exp.host/--/api/v2/push/send';

const pushNotificationRequestHeaders = {
	Accept: 'application/json',
	'Accept-Encoding': 'gzip, deflate',
	'Content-Type': 'application/json',
};
const FieldValue = require('firebase-admin').firestore.FieldValue;

module.exports = {
	admins,
	orgs,
	users,
	notifications,
	usersCollection,
	adminsCollection,
	notifsCollection,
	orgsCollection,
	userNotifsDocPath,
	expoNotificationServiceUrl,
	pushNotificationRequestHeaders,
	FieldValue,
};
