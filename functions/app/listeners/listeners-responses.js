const functions = require('firebase-functions');

const handleCreateResponsesCollection = functions.firestore
	.document(userNotifsDocPath)
	.onUpdate((snapshot, context) => {
		console.info(snapshot.before, snapshot.after);
	});

module.exports = handleCreateResponsesCollection;
