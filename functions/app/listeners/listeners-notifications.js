const functions = require('firebase-functions');
const fetch = require('node-fetch');

const {
  usersCollection,
  expoNotificationServiceUrl,
  pushNotificationRequestHeaders,
  userNotifsDocPath,
} = require('../constants');

async function getExpoDeviceToken(uid) {
  try {
    const userDocRef = usersCollection.doc(uid);
    const result = await userDocRef.get();
    return result.data().expoDeviceToken;
  } catch ({ name, message }) {
    console.error(`Status: ${name}: ${message}`);
  }
}

async function postRequest(uid) {
  try {
    const expoDeviceToken = await getExpoDeviceToken(uid);
    const data = {
      to: expoDeviceToken,
      title: 'New Message from Notifyd!',
      body: 'You have a new message from Notifyd. Check the app now!',
      sound: 'default',
      badge: '1',
    };

    const fetchResult = fetch(expoNotificationServiceUrl, {
      method: 'POST',
      headers: pushNotificationRequestHeaders,
      body: JSON.stringify(data),
    });

    return fetchResult;
  } catch ({ name, message }) {
    console.error(`Status: ${name}: ${message}`);
  }
}

const handleCreateNotification = functions.firestore.document(userNotifsDocPath).onCreate((snapshot, context) => {
  return console.log(context);
});

const handleCreateMessage = functions.firestore
  .document('/users/{uid}/notifications/{notificationId}')
  .onCreate((snapshot, context) => {
    const { uid, notificationId } = context.params;
    console.log(`UserId is: ${uid}`);
    console.log(`NotificationId is: ${notificationId}`);
    return postRequest(uid);
  });

module.exports = { handleCreateNotification, handleCreateMessage };
