function handleError({ status, name, message }) {
  return `Status: [${status}] Error: ${name}: ${message}`;
}

module.exports = {
  handleError,
}