const express = require('express');
const morgan = require('morgan');
const app = express();
const home = require('./routes/routes-home');
const responses = require('./routes/routes-responses');
const devices = require('./routes/routes-devices');

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
	morgan('dev', {
		skip: function(req, res) {
			return res.statusCode < 400;
		},
		stream: process.stderr, 
	}),
);

app.use(
	morgan('dev', {
		skip: function(req, res) {
			return res.statusCode >= 400;
		},
		stream: process.stdout,
	}),
);

app.use('/', home);
app.use('/responses', responses);
app.use('/devices', devices);

module.exports = app;
