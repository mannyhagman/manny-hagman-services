const functions = require('firebase-functions');
const fetch = require('node-fetch');

const {
  usersCollection,
  expoNotificationServiceUrl,
  userNotifsDocPath,
  orgsCollection,
  pushNotificationRequestHeaders,
  notifications,
  FieldValue,
} = require('../constants');

async function getExpoDeviceToken(uid) {
  console.debug(`Attempting to get expo device token for user: ${uid}`);
  try {
    const userDocRef = usersCollection.doc(uid);
    const result = await userDocRef.get();

    if (!result.data.expoDeviceToken) {
      console.debug(`User:${uid} does not have an expo device token`);
      return;
    }

    return result.data().expoDeviceToken;
  } catch (error) {
    console.error(error);
  }
}

async function sendPushNotification(uid) {
  try {
    const expoDeviceToken = await getExpoDeviceToken(uid);
    console.debug(`Attempting to send push notification to ${uid} with expoDeviceToken: ${expoDeviceToken}`);

    const data = {
      to: expoDeviceToken,
      title: 'New Message from Notifyd!',
      body: 'You have a new message from Notifyd. Check the app now!',
      sound: 'default',
      badge: '1',
    };

    const fetchResult = fetch(expoNotificationServiceUrl, {
      method: 'POST',
      headers: pushNotificationRequestHeaders,
      body: JSON.stringify(data),
    });

    return fetchResult;
  } catch ({ status, name, message }) {
    console.error(`Error: ${name}: ${message}`);
  }
}

const handleCreateNotification = functions.firestore.document(userNotifsDocPath).onCreate((snapshot, context) => {
  console.debug(`Notification data has been written to firestore...preparing listener action...`);
  const { uid, notificationId, orgId } = context.params;
  console.debug(`Current context params are ${context.params}`);
  const notifRef = orgsCollection.doc(orgId).collection(notifications).doc(notificationId);
  notifRef.update({ users: FieldValue.arrayUnion(uid) });
  console.debug(`preparing to send push notification to user:${uid}`);
  return sendPushNotification(uid);
});

module.exports = handleCreateNotification;
