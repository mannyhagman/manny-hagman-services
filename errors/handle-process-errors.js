module.exports = function handleProcessErrors() {
  process.on('uncaughtException', (error) => {
    console.error('Error:', error.message);
    process.exit(1);
  });
  process.on('unhandledRejection', (error) => {
    console.error('Error:', error.message);
  });
};
