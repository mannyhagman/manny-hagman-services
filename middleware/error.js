function handleError(error, req, res, next) {
  console.error(error);
  res.send({
    error,
  });
}

module.exports = handleError;
