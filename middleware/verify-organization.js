const { adminsCollection, orgs } = require('../config/constants');

async function verifyOrganization(req, res, next) {
  try {
    const { adminId } = req.body;

    const adminOrgDocRef = adminsCollection
      .doc(adminId)
      .collection(orgs)
      .doc(req.body.organization || req.params.organization);
    const adminOrgDocData = await adminOrgDocRef.get();

    if (!adminOrgDocData.exists) {
      console.error({
        code: 400,
        message: `Organization with id ${req.params.organization ||
          req.body.organization} does not exist or current EMR admin does not have access to organization data`,
      });
      res
        .status(400)
        .send(
          `Organization with id ${req.params.organization ||
            req.body.organization} does not exist or current EMR admin does not have access to organization data`,
        );
    } else {
      res.locals.organization = req.body.organization || req.params.organization;
      next();
    }
  } catch ({ name, message }) {
    console.error({
      code: 500,
      name,
      message,
    });
    res.send({
      code: 500,
      name,
      message,
    });
  }
}

exports.verifyOrganization = verifyOrganization;
