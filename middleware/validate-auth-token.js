const jwt = require('jsonwebtoken');

const { SECRET, adminsCollection } = require('../config/constants');

async function validateAuthToken(req, res, next) {
  try {
    if (!req.headers.authorization) {
      res.status(400).send('No authorization token provided');
      return;
    }

    const { authorization } = req.headers;
    const token = authorization.slice(7);
    const { id } = jwt.verify(token, SECRET);
    const adminsDoc = await adminsCollection.doc(id).get();
    if (!adminsDoc.exists) {
      res.status(400).send('Bad request: Invalid Token - Access Denied');
      return;
    } else {
      res.locals = req.body;
      res.locals.adminId = id;
      next();
    }
  } catch ({ name, message }) {
    res.send(`${name}: ${message}`);
  }
}

exports.validateAuthToken = validateAuthToken;
